import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'

import "./header.css";

const Header = ({ siteTitle }) => (
  <div
    style={{
      background: '#2c3e50',
      marginBottom: '1.45rem',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1.45rem 1.0875rem',
      }}
    >
    <table>
      <td>
        <h1 style={{ margin: 0 }}>
          <Link
            to="/"
            style={{
              color: 'white',
              textDecoration: 'none',
            }}
          >
            {siteTitle}
          </Link>
        </h1>
      </td>
      <td>
        <div className="dropdown">
          <h3>DCCs</h3>
          <div className="dropdown-content">
            <Link
              to="/dcc/prog-four/"
              style={{
                color: '#2F3E4E',
                textDecoration: 'none',
              }}
            >
              PROG
            </Link>
            <hr/>
            <Link
              to="/dcc/desn"
              style={{
                color: '#2F3E4E',
                textDecoration: 'none',
              }}
            >
              DESN
            </Link>
            <hr/>
            <Link
              to="/dcc/cfmg"
              style={{
                color: '#2F3E4E',
                textDecoration: 'none',
              }}
            >
              CFMG
            </Link>
          </div>
        </div>
        <div className="dropdown">
            <Link
              to="/dacs/"
              style={{
                color: "white",
                textDecoration: "none"
              }}
            >
              <h3>DACs</h3>
            </Link>
        </div>
        <div className="dropdown">
            <h3>Reflections</h3>
            <div className="dropdown-content">
            <Link
              to="/reflections/continuing-professional-development"
              style={{
                color: '#2F3E4E',
                textDecoration: 'none',
              }}
            >
              Continuing Professional Development
            </Link>
            </div>
        </div>
      </td>
    </table>
    </div>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: '',
}

export default Header
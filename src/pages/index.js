import React from "react"
import Layout from "../components/layout";
import { PropTypes } from "prop-types";
import { Paper, withStyles } from "@material-ui/core";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

function IndexPage(props) {
  const { classes } = props;

  return (
    <Layout>
      <h1>Welcome!</h1>
      <hr/>
      <Paper className={classes.root} elevation={3} >
        <h2>About Me</h2>
        <p>I am currently working as a developer at MLC, "National Australia Bank's wealth management business" (mlc.com.au, 2018). MLC provides superannuation, investments, and financial advice to corporate, institutional and retail costumers. My direct supervisor will be Adnan Keskin, a delivery manager for the digital content component of the business, he provides guidance and logistical structure to ensure that projects meet their deadlines, and serves as Scrum Master for multiple teams. </p>
        <p>I am working on both front and back end aspects of the MLC online website. As a developer in an agile team, my responsibilities involve improving and adding features to the website, as well as squashing bugs in the code.</p>
        <p>I secured my placement via an application through UTS Career Hub and two interviews, one aimed at determining whether my technical and personal skills would fit the roll, and a follow up to ensure that I would fit in culturally.</p>
        <p>I aim to achieve certain SFIA competencies, outlined in my skills inventory submission. I've gained experience using Spring MVC, Hibernate, Jasper Report, Velocity Templates, CucumberJs, the Perfecto testing suite, Oracle Database, and, of course, Java. I have done this on the job and in my own time and I am excited to explore and push my understanding of these topics even further. However, I am also eager to gain experience furthering my non-technical goals through exposure to new social/business situations. This could be as simple as meetings with superiors or casual conversations with my colleagues over lunch. </p>
        <p>Thus far I have found working in a professional industry environment to be fantastic, I thoroughly enjoying going in to work to solve different problems each day, and finding myself constantly adapting to new requirements or situations. No doubt over time this joy will fade. I have also found it a very humbling experience to be confronted quite directly with just how much I don't know and am yet to learn about software development. </p>
      </Paper>
      <hr/>
      <p>LinkedIn and GitHub links coming soon!</p>
      <div style={{ maxWidth: '300px', marginBottom: '1.45rem' }}>
      </div>
      <br/><br/>
      <div>Icons made by <a href="https://www.flaticon.com/authors/itim2101" title="itim2101">itim2101</a> from <a href="https://www.flaticon.com/" rel="noopener noreferrer"	title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </Layout>
  );
};

IndexPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles (styles)(IndexPage);


import React from "react";
import Layout from "../../components/layout";
import { withStyles, Paper } from "@material-ui/core";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  }
});

function ProgFourPage(props) {
  const { classes } = props;

  return (
    <Layout>
      <h2>
        Programming <br/>
        PROG, Level 4
      </h2>

      <Paper className={classes.root} elevation={3} >
        <p><i>
          Designs, codes, tests, corrects and documents complex programs and scripts from agreed specifications, 
          and subsequent iterations, using agreed standards and tools, to achieve a well-engineered result. 
          Takes part in reviews of own work and leads reviews of colleagues' work.  
        </i></p>
      </Paper>

      <hr/>

      <Paper className={classes.root} elevation={3} >
        <p>
          My current project requires the implementation of a variety of new field validations 
          (e.g. the first name field can only contain certain characters). Near the project’s inception, 
          I undertook a set of analysis stories to determine the areas of the codebase requiring changes, how this 
          existing code functions, and what the potential impacts of the changes could be. I reported my findings 
          to my development team, a technical business analyst and a solutions architect.  This allowed the team to 
          more accurately provide estimates for the actual implementation and testing of these forms and their new 
          validations. One of the questions I’d answered was “should we build a shared validation library?” My answer 
          was nuanced –the answer was dependant on our priorities, therefore allowing the team to make an informed decision.
        </p>
        <p>
          During the implementation phase I used my knowledge of Java and JUnit to overhaul the existing validation unit tests, 
          making them much more extensible and readable. To ensure that our unit testing framework met a high standard I sat 
          with one of my team mates for multiple code reviews to repeatedly iterate and improve upon the code structure. This 
          allowed our tester to create her own unit tests and substantially lighten the workload of integration tests. 
        </p>
        <p>
          The required validations almost always required specific characters to be included or excluded, so we used regular 
          expressions (regex). Some of these rules were shared across multiple system components, worked on by different team 
          members. I was the most experienced at using regex so the creation and sharing of these rules became my responsibility. 
          I created a page in our wiki for the project’s regular expressions, so that each team member could refer to them 
          whenever necessary. I kept this page updated as I improved on these rules, and the team was able to make their own 
          contributions. My teammates also often requested my input on regex problems that were specific to the functionality 
          they were implementing.
        </p>
        <p>
          As we neared the end of the project’s implementation, I was very efficient in fixing the bugs that arose, allowing 
          rapid turnover and retest of defects. An existing production defect was raised as part of testing, involving the 
          application crashing if the form submit button was clicked multiple times without editing any fields. The issue was 
          conceptually simple (a null pointer exception) but determining the root cause meant following a trail from the 
          backend (Java, Spring Webflow, JavaServer Pages), to the front end and the client-side logic (JavaScript, HTML/CSS). 
        </p>
        <p>
          Overall my contributions to this project demonstrate my programming abilities and have cemented my place as a 
          strong and contributing member of my team.
        </p>
      </Paper>
    </Layout>
  );
};

ProgFourPage.PropTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles (styles)(ProgFourPage);
import React from 'react'
import Layout from '../../components/layout'
import { Paper, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

function CfmgFivePage(props) {
  const { classes } = props;

  return (
    <Layout>
      <h2>
        Configuration Management <br/>
        CFMG, Level 5
      </h2>
      
      <Paper className={classes.root} elevation={3} >
          <p><i>
            Manages configuration items (CIs) and related information. Investigates and implements tools, techniques and processes for managing 
            CIs and verifies that related information is complete, current and accurate.    
          </i></p>
      </Paper>

      <hr/>

      <Paper className={classes.root} elevation={3} >
        <p>
          While working at MLC I have undertaken a variety of configuration tasks, primarily aligning MLC’s testing environments with those of other 
          teams in order to complete end-to-end testing. This has involved learning about and using a variety of tools, platforms and techniques, such 
          as Jenkins, TeamCity, Ansible, Puppet, and Amazon Web Services to name a few. I will often be asked to “point np19 (MLC environment) at 
          SIT 5 (downstream NAB environment)”, with no further direction or instruction. Completing this task requires knowledge of the environment 
          architecture and of a variety of tools. I will have to edit, save and deploy a change to Akamai, our content distribution network (CDN), 
          as well as making a change to some Ansible variables before redeploying our application stack using TeamCity. The final step is of course running a 
          quick test to ensure that our environment configuration has successfully been updated to align with that of the larger company. 
        </p>
        
        <p>
          I’ve also taken it upon myself to improve the automation of some file loading tasks, which requires the creation of new configuration items that 
          will allow file destinations to be updated dynamically and with greater ease, removing a lot of the potential for human error. I found some time 
          to create a prototype of this destination configuration using tools and a framework that I selected myself, and presented it to my manager, he 
          excitedly suggested that I present it to the designers of the file loading process. They generally approved of my new configuration design, 
          albeit with a few minor suggestions.  
        </p>
      </Paper>
    </Layout>
  );
};

CfmgFivePage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles (styles)(CfmgFivePage);
import React from 'react';
import Layout from '../../components/layout';
import { Paper, withStyles } from "@material-ui/core";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  }
});

function DesnFourPage(props) {
  const { classes } = props;

  return(
    <Layout>
      <h2>System Design<br/>
        DESN, Level 4
      </h2>

      <Paper className={classes.root} elevation={3} >
        <p><i>
          Recommends/designs structures and tools for systems which meet business needs and takes into account target 
          environment, performance security requirements and existing systems. Delivers technical visualization of 
          proposed applications for approval by customer and execution by system developers. Translates logical 
          designs into physical designs and produces detailed design documentation. Maps work to user specification 
          and removes errors and deviations from specification to achieve user-friendly processes.
        </i></p>
      </Paper>

      <hr/>
      
      <Paper className={classes.root} elevation={3} >
        <p>
          Working on a production defect led me to realise the potential benefit to the organisation if my team were 
          to centralise our search functionality. My manager agreed that I should investigate this further. 
          I took it upon myself to perform analysis and estimation of the work required to complete this project and 
          gave myself two weeks to come up with a reasonable pitch. I spoke with my colleagues about the feasibility of the 
          idea, early designs and time estimates. I spoke with a solutions architect to achieve a high-level understanding 
          of how my solution should work and potential system impacts. I engaged a variety of stakeholders including my manager, 
          the solutions architect, my colleagues and a few of the project managers and organised a meeting to pitch my project. 
          The project would make the codebase much more maintainable and extensible, allowing cost savings in the future, as well 
          as resolving multiple production defects, satisfying the business-oriented stakeholders.
        </p>
        <p>
          My pitch and the estimates I’d provided were enough to demonstrate that the proposed work was beneficial enough to warrant 
          funding. The project was estimated to take four sprints to complete. My design mimicked that of a microservice architecture, 
          which I had recently learnt about. My team was immediately onboard with the requirements, eager to work on new technology. 
          After it was decided that the project was to go ahead I spent time with my team’s technical business analyst to produce 
          stories that would allow the project to be completed on schedule.
        </p>
        <p>
          During the project I worked primarily on development, writing code and performing code reviews with and for my colleagues. 
          I organised small showcases to demonstrate the new search functionality to project stakeholders. I reconfigured existing 
          applications to utilise this new search service. I worked closely with our testers on test-driven development. I engaged 
          various parts of the business to organise environment changes, as well as assessing accessibility requirements 
          (the changes to the front-end were minimal in this case so this was straightforward). I spread myself out as described 
          above, because this was a project that I was excited to be a part of and because I was the one who had really had 
          the idea and done the research, so I wanted to see it turn out well. Working together and monitoring our velocity 
          we were able to complete the project in five sprints, just a bit over the estimate.
        </p>
        <p>
          In conclusion, the project turned out well and the work I put in was recognised at our next department meeting. 
          I was incredibly satisfied with the effort and the exposure I gained to different business components was a great 
          networking opportunity. My strong work ethic shone through and hopefully many of my colleagues will remember this for a long time.
        </p>
      </Paper>
    </Layout>
  );
};

DesnFourPage.PropTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles (styles)(DesnFourPage);
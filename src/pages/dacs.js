import React from "react";
import Layout from "../components/layout";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

function DacsPage(props) {
  const { classes } = props;

  return (
    <Layout>
      <h2>Developing Attribute Claims</h2>
      <hr/>
      <Paper className={classes.root} elevation={3} >
        {/* <Typography component="p"> */}
          <p>
            <b><u>
              Reflect on personal and professional experiences to engage in independent development beyond formal education for lifelong learning.
            </u></b>
          </p>
          <p><b>Level:</b> Proficient</p>
          <p>
            I have achieved this learning goal as demonstrated by the quantity and variety of reflection pieces I produced as part of this subject (Appendix A). The variety of and events on which these reflections have been based on has demonstrated my ability perform abstract reflections on concrete experiences. In combination with the changes I’ve made to my behavior resultant of my reflections this produces a feedback loop wherein I can constantly and independently develop my behaviors and abilities. Over time I can continue to assimilate new behaviors and abilities and discard flawed behaviors.     
          </p>
        {/* </Typography> */}
      </Paper>
        
      <hr/>

      <Paper className={classes.root} elevation={3} >
        {/* <Typography component="p"> */}
          <p>
            <b><u>
              Work as an effective member of diverse teams within a multi-level, multi-disciplinary and multi-cultural setting, communicate effectively in ways appropriate to discipline, audience and purpose.      
            </u></b>
          </p>
          <p><b>Level:</b> Competent</p>
          <p>
            Over the course of my employment I have had a variety of experiences and interactions with colleagues of various professional, cultural and technical backgrounds and have learnt a multitude of social rules and constructs present in the corporate world. How I interact with my colleagues is just as important as the content of the interaction. I have managed to not only develop effective professional relationships with my colleagues, but in many cases engage in satisfying personal relationships. I have run show the factor limiting me to a “competent” level of attainment is that I have been limited in the levels of interaction. I have not had many interactions with higher-level managers and as such have not had to learn the finer points of these types of interactions. As I spend more time in new and different scenarios with colleagues from a variety of backgrounds I am confident that I will attain a proficient level of experience in this attribute.
          </p>
        {/* </Typography> */}
      </Paper>
    </Layout>
  );
};

DacsPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles (styles)(DacsPage);
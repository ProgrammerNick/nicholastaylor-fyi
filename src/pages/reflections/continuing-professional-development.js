import React from "react";
import Layout from "../../components/layout";
import { Paper, withStyles } from "@material-ui/core";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

function ContDevPage(props) {
  const { classes } = props;

  return (
    <Layout>
      <h2>Continuing Professional Development <br/> 1/12/2018 </h2>
      <Paper className={classes.root} elevation={3} >
        <p>
          MLC uses Amazon Web Services (AWS) as its infrastructure provider, and as such all developer, DevOps, and architects are 
          expected to obtain AWS certification in one form or another, at MLC’s expense. A quick search of job boards is demonstrative 
          of the need for cloud skills in the modern tech industry, so I knew that this was a fantastic opportunity for me to make great 
          progress in my career independent of MLC. Not only this, but my manager has made it clear that he is willing to pay for other 
          certifications and learning materials should I desire them. In my personal life, I have also found it extremely beneficial to spend 
          time reading, prototyping, and generally messing around with new technologies (such as React), or to practice implementing concepts 
          that will benefit my professional career (such as setting this site up in AWS).    
        </p>
        <p>
          The encouragement at MLC to engage in professional technical development is rejuvenating, as oftentimes work can feel a bit 
          intimidating, especially when one considers the breadth of knowledge and skills that are required to be considered “experienced”.
          MLC’s encouragement is a reminder that no one knows everything, and that the company is acknowledging that and providing some focus 
          on the expected base skillset of the technical workers. This in turn makes me feel encouraged to both achieve AWS certification and 
          continue doing my own independent learning and prototyping.     
        </p>
        <p>
          My manager also appreciates my enthusiasm and acknowledges to me in our one-on-one catchup sessions that my approach to constantly 
          learning new skills, techniques, tools, etc. will be beneficial to my career.   
        </p>
        <p>
          One of my concerns is burnout. Spending 40 hours a week grinding away at work problems, only to come home and spend upwards of 
          15-20 hours on independent work can sometimes leave me feeling like I would rather jump off a bridge than spend another moment looking
          at a screen. 
        </p>
        <p>
          I recently read <a href="https://dev.to/jhotterbeekx/keep-learning-should-you-invest-your-own-time-22l_"> this article</a> in which
          it is recounted that Robert C. Martin (author of widely known books such as Clean Code: A Handbook of Agile Software) recommends 
          that programmers undertake at least 20 hours of independent learning and research per week. The article ultimately dismisses this statement, 
          comparing the workload other professions (law, medicine), and the independent work required of them, as well as what other industry 
          professionals have said (only 2 recommended 20+ hours of independent work), determining that this figure is not necessary after all. 
        </p>
        <p>
          I am finding it important to consider all viewpoints on this topic. Elon Musk recently 
          <a href="https://twitter.com/elonmusk/status/1067173497909141504?lang=en"> tweeted </a> “No one ever changed the world working 40 hours a week”. Questionable 
          ethics of his approach to managing his workers aside, the idea behind Musk’s statement is one that I find myself getting behind at this point 
          in my career. If I want to be fantastic at what I do, I need to work hard at it. Ultimately, I will have to find the balance between this and 
          the previous viewpoint. 
        </p>
        <p>
          What I have learnt that is generally applicable, however, is that hard work at work gets noticed and is often rewarded. 
          How much time I spend slacking off/engaging socially as a proportion of the time I spend at the office during the day is noticed, and 
          how much value I contribute to the overall team is also noticed. When I submit code for review, spending an extra 30 minutes to make sure 
          that it’s of a higher standard is often worth it. Now, the conundrum ahead is that ensuring this level of quality with more complicated 
          tools and technology will likely require at least a bit of independent study. This is essential, but how much is too much?
        </p>
      </Paper>
    </Layout>
  );
};

ContDevPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles (styles)(ContDevPage);